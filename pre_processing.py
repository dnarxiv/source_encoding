#!/usr/bin/python3

import argparse
import os
import subprocess
import math

import file_to_dna


"""
first step of dna storage
convert the files into dna sequences by :
adding files relative path as headers
zipping all files
splitting files that are too large for 1 assembly
"""

compression_type = "gzip" # set the used compression method


def zip_file(file_path, output_path):
    """
    compress the file and write it at the output path
    """
    
    if compression_type == "gzip":
        compression_command = "gzip -c9 "+ file_path + " > "+output_path
        subprocess.run('/bin/bash -c "$COMMAND"', shell=True, env={'COMMAND': compression_command})
        return
    
    if compression_type == "cmix":
        if not os.path.isfile(output_path):
            compression_command = "/udd/oboulle/Documents/result_analysis/compression_analysis/cmix/cmix -c "+ file_path + " "+output_path
            subprocess.run('/bin/bash -c "$COMMAND"', shell=True, env={'COMMAND': compression_command})
        else:
            print("already done",output_path)
        return
    # type not supported
    print("compression error, unknown format:",compression_type)
    exit(0)


def unzip_file(file_path, output_path):
    """
    uncompress the file and write it just where it is
    """
    
    if compression_type == "gzip":
        decompression_command = "gzip -d "+ file_path
        subprocess.run('/bin/bash -c "$COMMAND"', shell=True, env={'COMMAND': decompression_command})
        # move the unzipped file to the defined output path
        os.replace(get_uncompressed_name(file_path), output_path)
        return
    
    if compression_type == "cmix":
        decompression_command = "/udd/oboulle/Documents/result_analysis/compression_analysis/cmix/cmix -d "+ file_path + " "+output_path
        subprocess.run('/bin/bash -c "$COMMAND"', shell=True, env={'COMMAND': decompression_command})
        return
    # type not supported
    print("decompression error, unknown format:",compression_type)
    exit(0)

        
def get_compressed_name(file_path):
    
    if compression_type == "gzip":
        return file_path+".gz"
    
    if compression_type == "cmix":
        return file_path+".cx"
    # type not supported
    print("get_compressed_name error, unknown format:",compression_type)
    exit(0)
    
    
def get_uncompressed_name(file_path):
    
    if compression_type == "gzip":
        return file_path.replace(".gz", "")
    
    if compression_type == "cmix":
        return file_path.replace(".cx", "")
    # type not supported
    print("get_uncompressed_name error, unknown format:",compression_type)
    exit(0)
    

def insert_path_in_files(input_dir_path: str, rearanged_files_dir_path: str) -> None:
    """
    copy all files from a directory and paste them at the same level in output directory, with their relative path written before first line
    """
    for filename in os.listdir(input_dir_path):
        file_path = os.path.join(input_dir_path, filename)
        result_file_path = os.path.join(rearanged_files_dir_path, filename)
        
        # checking if it is a file
        if os.path.isfile(file_path):
            
            # get the relative path of the file
            header_line = str.encode("§*" + file_path + "\n") # add a special marker at the beginning of the header and encode it as bytes

            output_file_path = os.path.join(rearanged_files_dir_path, filename)
            
            while os.path.isfile(output_file_path): # test if a file with the same name is not already processed; can happen with files in other sub directories
                filename = "_" + filename # change the name
                output_file_path = os.path.join(rearanged_files_dir_path, filename)
            
            with open(file_path, "rb") as input_file: # copy the content as bytes so any type of file is supported
                bytes_lines = input_file.readlines()
            
            bytes_lines = [header_line] + bytes_lines # add the relative path in bytes as first line
            
            with open(output_file_path, "wb") as output_file: # write the resulting bytes
                output_file.write(b"".join(bytes_lines))

        elif os.path.isdir(file_path): # recursively call inside sub directories
            insert_path_in_files(file_path, rearanged_files_dir_path)
    

def compress_and_split(rearanged_files_dir_path: str, compressed_dir_path: str) -> None:
    """
    compress files, split in subfiles if too large, #TODO join small files together
    the objective is too create compressed files that fit in exactly in one storage molecule
    """
    
    max_binary_length = file_to_dna.get_max_binary_len() # get maximum length if binary payload that can fit in a molecule assembly

    files_compressed_size = {} # dict containing {path:size} of the files/subfiles whose compressed version fit in a molecule
    
    for filename in os.listdir(rearanged_files_dir_path):
        
        # compress the file
        file_path = os.path.join(rearanged_files_dir_path, filename)
        comp_file_path = get_compressed_name(os.path.join(compressed_dir_path, filename))
        
        # checking if it is a file
        if os.path.isfile(file_path):
            zip_file(file_path, comp_file_path)
            
        elif os.path.isdir(file_path):
            print("error pre processing (compress_all) : directory found in rearanged dir path", filename)
            exit(0)
            
        # get binary size of the compressed file
        binary_len = len(file_to_dna.convert_file_to_bits(comp_file_path))
        
        if binary_len <= max_binary_length: # if acceptable length, it's perfect
            files_compressed_size[filename] = binary_len # save the compressed size for this file
            continue
        else:
            # file too large, nedd to split it
            
            os.remove(comp_file_path) # delete the compressed file
            
            # read the original file as bytes
            with open(file_path, "rb") as input_file:
                bytes_lines = input_file.readlines()
            
            # extract the previously added header
            header_line = bytes_lines[0].decode().replace("\n", "")
            
            file_content = b"".join(bytes_lines[1:]) # join all the file bytes lines (without the header) for an equal split
                
            # establish a number of N required subfiles
            split_number = math.ceil(binary_len / max_binary_length)
            
            continue_split = True # if any of the N split files is still too big, re split in N+1 files
            
            while continue_split:
                continue_split = False
                # split the file
                print("split file",file_path,"in",str(split_number))

                bytes_per_split_file = math.ceil(len(file_content)/split_number) # get number of bytes per subfile
                
                bytes_contents_files = [] # list of split bytes of the file
                for i in range(split_number-1):
                    bytes_contents_files.append(file_content[i*bytes_per_split_file:(i+1)*bytes_per_split_file])
                bytes_contents_files.append(file_content[(split_number-1)*bytes_per_split_file:])
                
                # write each split bytes into a different subfile
                for i in range(split_number):
                    split_file_footer = "__"+str(i+1) # footer to add at the end of subfiles
                    split_file_bytes_content = bytes_contents_files[i]
                    
                    # generate a new header for the subfile, it has the path of the original file, + '__X/Y' with X the number of the subfile and Y the total number of subfiles for this file
                    split_header_line = header_line + split_file_footer + "/" + str(split_number) + "\n"
                    split_file_bytes_content = str.encode(split_header_line) + split_file_bytes_content
                    
                    split_file_path = file_path + split_file_footer
                    
                    with open(split_file_path, "wb") as f: # write the bytes content
                        f.write(split_file_bytes_content)
                    
                    compressed_subfile_path = get_compressed_name(get_uncompressed_name(comp_file_path) + split_file_footer)
                    # compress the split_file to the compressed directory
                    zip_file(split_file_path, compressed_subfile_path)

                    # check the size of the subfile
                    binary_len = len(file_to_dna.convert_file_to_bits(compressed_subfile_path))
        
                    if binary_len > max_binary_length: # if any of the subfiles is still too big
                        print(compressed_subfile_path, "still too big", str(binary_len))
                        # need to re split the original file in N+1 parts
                        continue_split = True
                        split_number += 1
                        # no need to remove old splits, they are overwritten
                        break
                    else:
                        files_compressed_size[filename + split_file_footer] = binary_len # save the compressed size for this file
                        # if the sub file is recreated, the new size will overwrite this one

    def merge_short_files(files_compressed_size: dict):
        """
        each molecule can store a maximum fixed number of bits
        since it's more efficient to have a few long molecule than a lot of short molecule,
        small files can be merged in one same molecule
        """
    
        #sorted_files_sizes = sorted(files_compressed_size.items(), key=lambda item: item[1], reverse=True) # sort dict by sizes from highest to lowest
        
        #files_compressed_size_bis = {}
        
        #merged_files_paths = []
        
        new_merge = False # set to true if at least one new merge has been made
        
        for i, filename in enumerate(list(files_compressed_size.keys())[:-1]):
            file_compressed_size = files_compressed_size[filename] # get size of the compressed file
            
            if file_compressed_size is None or file_compressed_size > max_binary_length: # impossible to merge because too large, or has already be used in a merge (set to None)
                continue # skip this file
            
            for filename_2 in list(files_compressed_size.keys())[i+1:]:
                file_compressed_size_2 = files_compressed_size[filename_2]
                
                if file_compressed_size_2 is None or file_compressed_size + file_compressed_size_2 > max_binary_length:
                    continue # skip this file
                
                # the sum of the 2 compressed files is lower than what can be stored,
                # so the original files will be merged and recompressed,
                # the compression of a merging is supposed to be smaller than the sum of compressions of each file
                
                # get the binary content of each file
                with open(os.path.join(rearanged_files_dir_path, filename), "rb") as input_file:
                    bytes_content = b"".join(input_file.readlines())
                with open(os.path.join(rearanged_files_dir_path, filename_2), "rb") as input_file:
                    bytes_content_2 = b"".join(input_file.readlines())
                    
                # remove the "merged_" from the name of already merged files for visibility
                merged_file_name = "merged_" + filename.replace("merged_","") + "+" + filename_2.replace("merged_","") 
                merged_file_path = os.path.join(rearanged_files_dir_path, merged_file_name)
                compressed_merged_file_path = get_compressed_name(os.path.join(compressed_dir_path, merged_file_name))
                print("new merge :",filename,"and",filename_2)
                
                with open(merged_file_path, "wb") as f: # write the sum of bytes content
                    f.write(bytes_content + bytes_content_2)
                
                # compress the merged file created
                zip_file(merged_file_path, compressed_merged_file_path)
                
                # test its size just in case, but it should fit in a molecule
                merged_binary_len = len(file_to_dna.convert_file_to_bits(compressed_merged_file_path))
    
                if merged_binary_len >= max_binary_length: 
                    print("error merging result too large", compressed_merged_file_path)
                    exit(0)
                
                # add the merged file to the dict because it can still be used for other merging if it's short enough
                files_compressed_size[merged_file_name] = merged_binary_len
                
                # remove the 2 old compressed files of the 2 files
                os.remove(os.path.join(compressed_dir_path, get_compressed_name(filename)))
                os.remove(os.path.join(compressed_dir_path, get_compressed_name(filename_2)))
                
                # set the compressed size of the 2 files to None to avoid them to be reused for merging
                files_compressed_size[filename] = None
                files_compressed_size[filename_2] = None
                
                new_merge = True # keep in memory that at least one new merge has been made in this loop
                
                break # leave the second loop, but others merges can still be done in the continuation of the first loop
                
        # continue to try to create other merging if at least one merge has been made
        if new_merge:
            print("continue merging...")
            print(files_compressed_size)
            merge_short_files(files_compressed_size)
        # otherwise, the loop can end since it no longer find possible merges   
        
    print(files_compressed_size)
    merge_short_files(files_compressed_size)



def convert_to_sequence(compressed_dir_path, payload_fragments_dir_path):
    """
    convert every compressed file into a dna sequence already sized for block design
    """
    # loop over compressed files
    for filename in os.listdir(compressed_dir_path):
        file_path = os.path.join(compressed_dir_path, filename)
        # checking if it is a file
        if os.path.isfile(file_path):
            
            output_file_path = os.path.join(payload_fragments_dir_path, get_uncompressed_name(filename))

            dna_sequence = file_to_dna.encode_file(file_path, output_file_path) # convert binaries into a dna sequence and save result in the output file

        elif os.path.isdir(file_path):
            print("error pre processing (convert to sequence) : directory found in compressed dir path", filename)
            exit(0)


def pre_processing(input_dir_path, output_dir_path):
    """
    main function, perform the pre processing steps
    """
    
    # insert relative path of files as a header
    rearanged_files_dir_path = output_dir_path + "/0_rearanged"
    try:
        os.mkdir(rearanged_files_dir_path)
    except OSError as error:
        print(error)
    
    insert_path_in_files(input_dir_path, rearanged_files_dir_path)
    
    
    # apply compression algorithm
    compressed_dir_path = output_dir_path + "/1_compressed"
    try:
        os.mkdir(compressed_dir_path)
    except OSError as error:
        print(error)
    
    compress_and_split(rearanged_files_dir_path, compressed_dir_path)
    
    
    # convert the binaries into a dna sequence for each subfile
    payload_fragments_dir_path = output_dir_path + "/2_payload_fragments"
    try:
        os.mkdir(payload_fragments_dir_path)
    except OSError as error:
        print(error)
    
    convert_to_sequence(compressed_dir_path, payload_fragments_dir_path)
    
    
# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='pre process files for dna storage before binary conversion')
    parser.add_argument('-i', action='store', dest='input_dir_path', required=True,
                        help='path to input directory of files to store')
    parser.add_argument('-o', action='store', dest='output_dir_path', required=True,
                        help='path to the output directory')


    # ---------- input list ---------------#
    arg = parser.parse_args()
    

    print("pre-processing files...")
    
    pre_processing(arg.input_dir_path, arg.output_dir_path)
        
    print("\tcompleted !")

