import sys
import os
import inspect
import math

import binary_dna_conversion as bdc
import hashing

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr

"""
Can be used to convert any type of file
use the byte encoding of the file to convert to a dna sequence
the file need to have a round number of byte
a single substitution error in the sequences can lead to a corrupted file (except for .txt files)
"""

CHECK_SUM_SIZE = 20 # number of bits used for the checksum

# biological constraints for block assembly
buffer_size = 15
primer_size = 20
overhang_size = 4
bsaI_size = 7

extern_extremity_size = primer_size + overhang_size + bsaI_size + buffer_size # size of non-payload dna part at the extern extremities (start of first block and end of last block)
intern_extremity_size = overhang_size + bsaI_size + buffer_size # size of non-payload dna part at the extremities between blocks
  
max_total_block_size = 300 # maximum allowed size for a complete block
min_total_block_size = 300 # minimum allowed size for a complete block
n_block_max = 10 # maximum assembled number of blocks



def set_block_size(new_size: int):
    """
    change the value of the blocks size
    """
    global max_total_block_size
    global min_total_block_size
    max_total_block_size, min_total_block_size = new_size, new_size
    

def compute_check_sum(binary_string: str) -> str:
    """
    calculate the check_sum binary value associated with the binary string
    """
    
    # split the string into substrings of CHECK_SUM_SIZE
    splitted_binary_string = [binary_string[i:i+CHECK_SUM_SIZE] for i in range(0, len(binary_string), CHECK_SUM_SIZE)]
    
    bin_sum = "0"
    for binary_split in splitted_binary_string:

        int_sum = int(bin_sum, 2) ^ int(binary_split,2) # XOR sum
        bin_sum = str(bin(int_sum))[2:] # convert back into binary
        
    return bin_sum.zfill(CHECK_SUM_SIZE) # fill the beginning with 0 to get the correct size


def apply_binary_filter(binary_string: str) -> str:
    """
    apply a filter to the binary string
    use the length of the string as a default hash key, so 2 files of different size have a different hash key
    this is because some zip results starts with the same bytes
    we just need to use the same key for the un-hashing
    """
    filter = hashing.hash_string_to_formated_base2(str(len(binary_string)), len(binary_string))
    filtered_binary_string = ""
    for i in range(len(binary_string)):
        filter_bit = int(filter[i]) # get filter bit of index i
        filtered_binary_string += str((int(binary_string[i])+filter_bit) % 2) # get reverse binary string of index i and XOR it with the filter bit
        
    return filtered_binary_string

  
def convert_file_to_bits(input_path: str) -> str:
    """
    convert any type of file into a binary string
    """
    # read the lines as bytes
    with open(input_path, "rb") as f:
        byte_list = []
        byte = f.read(1)
        while byte:
            byte_list.append(bin(ord(byte)))
            byte = f.read(1)
    # convert the bytes into binaries
    binary_string = ""
    for x in byte_list:
        binary_char = x[2:]
        # add some 0 at the beginning of binaries shorter than 8
        while len(binary_char) < 8:
            binary_char = "0"+binary_char
        binary_string += binary_char
    return binary_string


def get_max_binary_len() -> int:
    """
    get the maximum possible size of a binary payload that can fit in an assembly
    """
    
    max_total_dna_len = n_block_max * max_total_block_size # number of bases of a total assembly
    
    max_payload_dna_len = max_total_dna_len - 2*extern_extremity_size - 2*(n_block_max-1)*intern_extremity_size # without the assembly stuff
    
    max_binary_len = bdc.size_binary_from_dna_len_abaab(max_payload_dna_len) - CHECK_SUM_SIZE # convert the remaining bases to binary and remove the checksum bits
    
    return math.floor(max_binary_len) # round down


    
def encode_file(input_path: str, output_path: str) -> None:
    """
    convert any type of file into a list of dna_sequence pre sized for block assembly and save it in a .fasta format file
    encode the resulting dna sequence corresponding to the file payload
    adjust the length of binary by adding '0' at the start to get a round number of blocks
    objective is to fit the payload in the lowest number of blocks, but all blocks must be the same size after addition of non coding stuff (primer/buffer/overhang/bsaI) 
    """
    
    binary_string = convert_file_to_bits(input_path) # get the binary string representing the file content
    #binary_string = input_path #TODO REMOVE

    if bdc.forbidden_rest_abaab(len(binary_string) + CHECK_SUM_SIZE):
        # add a non coding 0 when size of incompatible length for dna conversion
        binary_string = "0" + binary_string
        
    dna_payload_size = bdc.size_dna_from_bit_len_abaab(len(binary_string) + CHECK_SUM_SIZE) # length of the payload after conversion in dna
    #print("payload size",str(dna_payload_size))
    
    # estimate number of blocks, round up to next int
    block_number = math.ceil((dna_payload_size + 2*extern_extremity_size - 2*intern_extremity_size) / (max_total_block_size - 2*intern_extremity_size))
    
    #print("number of blocks",str(block_number))
    if block_number > n_block_max: # should not occur as large files are split in pre processing
        print("error file to dna : block number too high :",str(block_number),"for",input_path)
        exit(1)
    
    # size of non payload dna part that is contained in the blocks and used for the assembly
    dna_for_assembly_size = 2*extern_extremity_size + 2*(block_number-1)*intern_extremity_size
    
    # estimate the total dna sequence length after addition of non coding parts
    total_dna_size = dna_payload_size + dna_for_assembly_size
    #print("total_dna_size",str(total_dna_size))
        
    # round the number of base per blocks, make sure it is above the minimal block size
    final_assembly_size = block_number * max(math.ceil(total_dna_size / block_number), min_total_block_size)
        
    # case when some bits needs to be added to increase the number of bases
    if final_assembly_size != total_dna_size:
    
        # calculate the number of bases to add to the payload to get a round number of equal length blocks
        dna_payload_needed_size = final_assembly_size - dna_for_assembly_size
        #print(bdc.size_binary_from_dna_len_aabaabb(dna_payload_needed_size) )
        # get number of bits to add
        filler_length = bdc.size_binary_from_dna_len_abaab(dna_payload_needed_size) - len(binary_string) - CHECK_SUM_SIZE
        #print("dna_payload_needed_size",str(dna_payload_needed_size)," +",filler_length,"bits")
    
        # fill with '0' at the beginning of the binary # not the end because some zip can end with octets of 0, which makes difficult to remove only the non coding '0'
        binary_string = math.ceil(filler_length) * "0" + binary_string
        #print("updated binary size", str(len(binary_string)))
        
    
    binary_string = binary_string[::-1] # reverse the binary string, because 2 files can have the same start with ziping methods
    
    # apply a filter to the binary string -> shuffle the data to avoid long rows of 0 or 1, and avoid rows repetitions 
    filtered_binary_string = apply_binary_filter(binary_string)
    
    
    # calculate and add the binary check_sum at the end
    binary_check_sum = compute_check_sum(filtered_binary_string)
    filtered_binary_string += binary_check_sum
        
    # convert binaries into dna sequence
    sequence = bdc.binary_to_dna_abaab(filtered_binary_string)
        
    total_sequence_size = len(sequence)+ dna_for_assembly_size
    
    # test for errors that should never occur (I hope ...)
    # round number of blocks, no blocks too large, no blocks to small
    if round(total_sequence_size/block_number) != total_sequence_size/block_number or total_sequence_size/block_number > max_total_block_size or total_sequence_size/block_number < min_total_block_size:
        print("error file to dna", input_path)
        print("\tseq payload size",str(len(sequence)))
        print("\ttotal estimated seq size",total_sequence_size)
        print("\t",str(block_number),"blocks of",str(total_sequence_size/block_number))
        exit(1)
    #print("\t",str(block_number),"blocks of",str(total_sequence_size/block_number))

    # split the sequence into blocks of correct size to add the non payload stuff later # start the block count at 1
    sub_sequences_dict = {}
    
    if block_number == 1:
        sub_sequences_dict["1"] = sequence
    elif block_number == 2:
        sub_sequences_dict["1"] = sequence[:len(sequence)//2]
        sub_sequences_dict["2"] = sequence[len(sequence)//2:]
    else:
        # size of blocks for 3/more blocks assembly
        size_extremity_block = total_sequence_size//block_number - extern_extremity_size - intern_extremity_size
        size_intern_block = total_sequence_size//block_number - 2*intern_extremity_size
        
        sub_sequences_dict["1"] = sequence[:size_extremity_block]
        index_sequence = size_extremity_block
        for i in range(1, block_number-1):
            sub_sequences_dict[str(i+1)] = sequence[index_sequence:index_sequence+size_intern_block]
            index_sequence += size_intern_block
            
        sub_sequences_dict[str(block_number)] = sequence[index_sequence:]
    
    # add number and blocks size to path, add type
    output_path = output_path + "_" + str(block_number) + "x" + str(total_sequence_size//block_number) + ".fasta"
    
    #return sequence #TODO REMOVE
    
    dfr.save_dict_to_fasta(sub_sequences_dict, output_path)
    
    

def decode_file(input_path: str, output_path: str) -> None:
    """
    convert a dna_sequence into the original file, assuming no errors in the sequence
    input : fasta format file with the payload sequences extracted from the block assembly
    """

    sequences_dict = dfr.read_fasta(input_path) # get all the sequences from the fasta file
    
    for seq_name, sequence in sequences_dict.items():
    
        # convert the dna sequence into a binary string
        binary_from_dna_string = bdc.dna_to_binary_abaab(sequence)
        
        if not binary_from_dna_string:
            print("warning file conversion, decoding an empty sequence",seq_name,"in",input_path)
            continue
    
        # test if the check_sum corresponds to the binary string
        binary_string = binary_from_dna_string[:-CHECK_SUM_SIZE]
        binary_check_sum = binary_from_dna_string[-CHECK_SUM_SIZE:]
        
        if compute_check_sum(binary_string) != binary_check_sum:
            print(compute_check_sum(binary_string),"!=",binary_check_sum)
            print("Invalid check sum for",seq_name,"in",input_path)
            continue
            
        # apply the same filter used in the encoding to the binary string to remove it  
        binary_string = apply_binary_filter(binary_string)
        binary_string = binary_string[::-1] # reverse the binary string to get the original
    
        # case binaries length is not multiple of 8 -> remove the excess bits at the beginning that have been added in the encoding to get a round number of blocks
        rest = len(binary_string) % 8
        if rest != 0:
            binary_string = binary_string[rest:]
    
        # remove octets of zeros at the beginning (the start of the sequence can be filled with zeros to get a round number of blocks)
        while binary_string.startswith(8*"0"): # 1/256 (2**8) chance to remove actual data ! but 8*0 is ascii char NULL
            binary_string = binary_string[8:]       
        
        # convert binaries into bytes
        n = int(binary_string, 2)
        bytes = n.to_bytes((n.bit_length() + 7) // 8, 'big')
        
        # write the bytes into the file
        with open(output_path, "wb") as f:
            f.write(bytes)
        return # end the decoding, since the sequence passed the checksum


# =================== main ======================= #
if __name__ == '__main__':
    #doc_path = sys.argv[1]
    #binary_string = sys.argv[1]
    #seq = encode_file(doc_path)
    
    #exit(0)

    for i in range(400,10000, 8):
        binary = i*"1"
        if len(binary) % 2 != 0:
            continue
        
        print("i=",str(i))
        seq = encode_file(binary, "")
        binary_result = decode_file(seq, "")
        
        if binary != binary_result:
            print(binary)
            print(seq)
            print(binary_result)
            exit(0)
    #encode_file("", "test")
    #seq = binary_to_dna_abaab(binary_string)
    #print(seq)
    #print(dna_to_binary_abaab(seq))
    

