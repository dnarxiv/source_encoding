import sys
import math
import random

import sequence_control

"""
Used to convert binary to dna and reverse
"""

# pair of bit to nucleotide
bit_pair_to_dna = {"00": "A", "01": "G", "10": "T", "11": "C"}
# "_" is for missing nucleotides
dna_to_bit_pair = {"A": "00", "G": "01", "T" :"10", "C": "11", "_" :"00"}

bit_to_dna_AT = {"0": "A", "1": "T"}
bit_to_dna_GC = {"0": "G", "1": "C"}

dna_to_bit_ATGC = {"A": "0", "T": "1", "G": "0",  "C": "1", "_": "0"}


def bit_to_dna_balance_GC(bit_char: str, reference_base: str):
    """
    convert a bit to a dna base to balance the GC with a reference base 
    """
    if reference_base in ["A", "T"]:
        return bit_to_dna_GC[bit_char]
    else:
        return bit_to_dna_AT[bit_char]


def forbidden_rest_abaab(binary_string_size: int) -> bool:
    """
    return True if the binary is of a length that cannot be converted by the abaab method
    """
    return binary_string_size % 8 in [1, 4, 6]


def binary_to_dna_abaab(binary_string: str) -> str:
    """
    convert binaries into dna sequence with some properties
    the binaries are divided into parts of 8 bits
    for each 8 bits part, 1st 2nd bits are normally converted into dna : (00:A, 01:G, 10:T, 11:C)
                          3rd bit is converted depending on previous conversion : if previous in {AT}, then (0:G, 1:C), elif in {GC}, then (0:A, 1:T)
                          4th 5th bits are normally converted into dna
                          6th 7th bits are normally converted into dna
                          8th bit is converted depending on previous conversion : if previous in {AT}, then (0:G, 1:C), elif in {GC}, then (0:A, 1:T)
    this ensures that the total sequence cannot contain homopolymers > 3,
    the GC% is in [40%, 60%] in a window of 5 and the conversion rate is 1.6 bit/base
    
    warning : need the binary string to be a multiple of 2, or rest is inconsistent with decoding 
    ex : a rest of 0 -> A, a rest of 00 -> A
    so how to decode A ?
    only allowing rests multiple of 2 removes ambiguity
    
    #TODO warning : if ending with bsaI -> end can currently be aa when a rest of 4 bits -> will break with banword removal

    """
    if forbidden_rest_abaab(len(binary_string)):
        print("error binary dna conversion, need a binary string multiple of 8, or with a rest of 2, 3, 7 ("+str(len(binary_string) % 8)+")")
        exit(0)
    
    sequence = ""
    n_octets, rest = divmod(len(binary_string), 8)
    for i in range(0, n_octets):
        bits8 = binary_string[i*8:(i+1)*8]
        nucleotide_1 = bit_pair_to_dna[bits8[0:2]]
        
        nucleotide_2 = bit_to_dna_balance_GC(bits8[2], nucleotide_1)
        
        nucleotide_3 = bit_pair_to_dna[bits8[3:5]]
        nucleotide_4 = bit_pair_to_dna[bits8[5:7]]
        
        nucleotide_5 = bit_to_dna_balance_GC(bits8[7], nucleotide_4)

        sequence += nucleotide_1 + nucleotide_2 + nucleotide_3 + nucleotide_4 + nucleotide_5
    # rest should be 0 because all documents contains a round number of octet
    # but some "0" can be added to fill the fragments
    bit_rest = binary_string[n_octets*8:n_octets*8+rest] # rest is 0-2-3-5-7 bits

    # last conversions depends on the length of the rest, 2 bits = 1 base; 3 bits = 2 bases; 5 bits = 3 bases; 7 bits = 4 bases
    if len(bit_rest) >= 2:
        nucleotide_1 = bit_pair_to_dna[bit_rest[0:2]] # nucleotide_1 from a pair of bits
        sequence += nucleotide_1
        if len(bit_rest) >= 3:
            # nucleotide_2 from a single bit and depending on nucleotide 1
            sequence += bit_to_dna_balance_GC(bit_rest[2], nucleotide_1)
            if len(bit_rest) >= 5:
                # nucleotide_3 from a pair of bits
                sequence += bit_pair_to_dna[bit_rest[3:5]] 
                if len(bit_rest) == 7:
                    # nucleotide_4 from a pair of bits
                    sequence += bit_pair_to_dna[bit_rest[5:7]] 

    # remove the banned words
    sequence_wo_banwords = remove_ban_words_abaab_encoding(sequence)
    
    return sequence_wo_banwords
    

def dna_to_binary_abaab(sequence: str) -> str:
    """
    convert back a sequence to binaries (opposite of binary_to_dna_abaab)
    """
    
    binary_string = ""
    n_quintuplet, rest = divmod(len(sequence), 5)
    for i in range(0, n_quintuplet):
        nucleotides5 = sequence[i*5:(i+1)*5]
        bits_12 = dna_to_bit_pair[nucleotides5[0]]
        bit_3 = dna_to_bit_ATGC[nucleotides5[1]]
        bits_45 = dna_to_bit_pair[nucleotides5[2]]
        bits_67 = dna_to_bit_pair[nucleotides5[3]]
        bit_8 = dna_to_bit_ATGC[nucleotides5[4]]
        binary_string += bits_12 + bit_3 + bits_45 + bits_67 + bit_8
    
    # handle rest < 5
    sequence_rest = sequence[n_quintuplet*5:n_quintuplet*5+rest]
    if len(sequence_rest) >= 1: # 1 base -> 2 bits
        binary_string += dna_to_bit_pair[sequence_rest[0]]
        
        if len(sequence_rest) >= 2: # 2 bases -> 2 bits + 1 bit
            binary_string += dna_to_bit_ATGC[sequence_rest[1]]

            if len(sequence_rest) >= 3: # 2 bases -> 2 bits + 1 bit + 2 bits
                binary_string += dna_to_bit_pair[sequence_rest[2]]

                if len(sequence_rest) == 4: # 4 bases -> 2 bits + 1 bit + 2 bits + 2 bit
                    binary_string += dna_to_bit_pair[sequence_rest[3]]
    
    return binary_string


def remove_ban_words_abaab_encoding(sequence: str, abaab_method_offset=0) -> str:
    """
    remove banned words from a sequence encoded with the binary_conversion.binary_to_dna_abaab() method
    the abaab method ensure a GC% in [40;60] on a window of 5 bases, but we the constraint is on larger windows
    changing a base to avoid a banned word should not affect the whole sequence too much
    the changed base must be a beta base, and be A<=>G; T<=>C, so the decoding of the updated sequence will still return the same result
    
    if the original sequence has already been fragmented, the 5 bases windows of the abaab method can be offset, abaab_method_offset is used to correct this
    """
    
    # change a beta base but keep the binary meaning for the decoding
    dna_change_beta = {"A": "G", "T": "C", "G": "A", "C": "T"}

    # check for banned words
    banword_indexes_list = [] # list of index start;index_stop of all banwords in the sequence
    for banned_word in sequence_control.get_forbidden_sequences():
        index = sequence.find(banned_word)
        while index != -1:
            banword_indexes_list.append([index, index+len(banned_word)])
            index = sequence.find(banned_word, index+1)
    
    # indexes of beta bases in the sequence (2nd and 5th base every 5 base window) # range start in negative for cases where the offset is > 0
    beta_indexes = [ abaab_method_offset+ 5*k+i for k in range(-1, math.ceil(len(sequence)/5)) for i in [1,4] ]

    sequence_wo_bans = sequence
    #print(len(sequence))
    #print(beta_indexes)
    #print(banword_indexes_list)

    for banword_index in banword_indexes_list:
        # indexes of beta bases that can be changed to remove a banned word
        potential_changes_index = [ k for k in beta_indexes if k >= banword_index[0] and k < banword_index[1] ]

        for change_index in potential_changes_index:
            sequence_wo_bans_list = list(sequence_wo_bans) # turn into list because python strings are immutable
            
            # change a beta_base without changing the meaning of encoded data
            sequence_wo_bans_list[change_index] = dna_change_beta[sequence_wo_bans[change_index]]
            
            index_before_ban_word = max(0, banword_index[0]-3) # test the changed part from 3 bases before to avoid creation of homopolymers
            index_after_ban_word = min(len(sequence)-1, banword_index[1]+3) # test to 3 bases after
            
            changed_sub_sequence = ''.join(sequence_wo_bans_list)[index_before_ban_word:index_after_ban_word]

            # test the sequence only for the banned words and homopolymers, ignore the GC%
            if sequence_control.sequence_check(changed_sub_sequence, window_size=60, min_GC=0, max_GC=100, verbose=False):   
                # keep the change that removes this ban word if it doesn't create homopolymers or other ban words            
                sequence_wo_bans = ''.join(sequence_wo_bans_list)
                #print("removed ban word", sequence[banword_index[0]:banword_index[1]],"->",changed_sub_sequence)
                break
            else:
                #print("failed to remove ban word", sequence[banword_index[0]:banword_index[1]],"-X>",changed_sub_sequence)
                #print("trying again...")
                pass
    
    # last check for very very odd cases (maybe overlapping ban words)
    for banned_word in sequence_control.get_forbidden_sequences():
        if banned_word in sequence_wo_bans:
            print("binary conversion: unable to remove forbidden sequences", sequence_wo_bans)
            exit(1)
    
    return sequence_wo_bans
   
    
def size_dna_from_bit_len_abaab(bit_length):
    """
    assume binary to dna_abaab conversion
    get the size the dna sequence will be from the given binary string length
    conversion rate is 1.6 bit/base
    """
    base_length = 5 * (bit_length//8) # full conversion of octets
    rest = bit_length % 8
    if rest == 0:
        return base_length
    if rest == 2:
        return base_length + 1
    if rest == 3:
        return base_length + 2
    if rest == 5:
        return base_length + 3
    if rest == 7:
        return base_length + 4
    print("error size_of_dna_from_bit_len_abaab : invalid size rest :",str(bit_length),"(rest of",str(rest)+")")
    exit(1)



def size_binary_from_dna_len_abaab(dna_length):
    """
    assume dna_abaab to binary conversion
    get the length the binary string will be from the given dna sequence
    conversion rate is 1.6bit/base
    """
    base_length = 8 * (dna_length//5) # full conversion of octets
    rest = dna_length % 5
    if rest == 0:
        return base_length
    if rest == 1:
        return base_length + 2
    if rest == 2:
        return base_length + 3
    if rest == 3:
        return base_length + 5
    if rest == 4:
        return base_length + 7


def forbidden_rest_baa(binary_string_size: int) -> bool:
    """
    return True if the binary is of a length that cannot be converted by the baa method
    """
    return binary_string_size % 5 in [2, 4]


def binary_to_dna_baa(binary_string: str, GC_window=20) -> str:
    """
    convert binaries into dna sequence with some properties
    the binaries are divided into parts of 5 bits
    for each 5 bits part, 1st bit is converted depending on previous converted sequence :
                            (0:G, 1:C) or (0:A, 1:T), first to break potential homopolymer, if not then to adjust %GC of the local sequence in a defined window  
                          2nd 3rd bits are normally converted into dna : (00:A, 01:G, 10:T, 11:C)
                          4th 5th bits are normally converted into dna : (00:A, 01:G, 10:T, 11:C)

    this ensure that the total sequence cannot contain homopolymers > 3,
    the GC% is in [33%, 66%] in the worst case in a window of 3, but tends to 50% with the adjustments and the conversion rate is 1.66 bit/base
    
    warning : need the binary string to be a multiple of 5, or rest is inconsistent with decoding 
    ex : a rest of 0 -> A, a rest of 00 -> A
    so how to decode a base A ?
    only allowing rests multiple of 5 removes ambiguity (also possible 0, 1 or 3 modulo 5)

    """
    if forbidden_rest_baa(len(binary_string)):
        print("error binary dna conversion, need a binary string multiple of 5, or with a rest of 1, 3 ("+str(len(binary_string) % 5)+")")
        return
    
    sequence = ""
    n_quintuplets, rest = divmod(len(binary_string), 5)
    for i in range(0, n_quintuplets):
        bits5 = binary_string[i*5:(i+1)*5]
        
        nucleotide_2 = bit_pair_to_dna[bits5[1:3]] # first alpha base of the encoded triplet
        nucleotide_3 = bit_pair_to_dna[bits5[3:5]] # 2nd alpha base
        
        # make nucleotide 1
        if len(sequence) >= 3:
            # check 3 previous encoded bases and 2 following alpha bases to see if there is a potential homopolymer of 4+
            if sequence[-3] == sequence[-2] == sequence[-1] or sequence[-2] == sequence[-1] == nucleotide_2 or sequence[-1] == nucleotide_2 == nucleotide_3:
                # break the homopolymer
                nucleotide_1 = bit_to_dna_balance_GC(bits5[0], sequence[-1])

            else:
                # if no homopolymer to break, adjust GC% in the window of preceding bases + 2 following alpha bases
                check_window = sequence[-min(len(sequence), GC_window):] + nucleotide_2 + nucleotide_3
                if check_window.count("A")+check_window.count("T") > check_window.count("G")+check_window.count("C"):
                    nucleotide_1 = bit_to_dna_GC[bits5[0]] # more A/T -> add a G/C
                elif check_window.count("A")+check_window.count("T") < check_window.count("G")+check_window.count("C"):
                    nucleotide_1 = bit_to_dna_AT[bits5[0]] # less A/T -> add a A/T
                else: # case of strictly equal %GC, just balance with following base
                    nucleotide_1 = bit_to_dna_balance_GC(bits5[0], nucleotide_2)
    
        else: # empty string, just balance the GC with the following base
            nucleotide_1 = bit_to_dna_balance_GC(bits5[0], nucleotide_2)
        
        sequence += nucleotide_1 + nucleotide_2 + nucleotide_3 # add the encoded b a a bases to the sequence

    # all complete quintuplets have been encoded, time for the rest
    bit_rest = binary_string[n_quintuplets*5:n_quintuplets*5+rest] # rest is 0-1-3 bits long

    if len(bit_rest) > 0:
    # last conversions depends on the length of the rest, 1 bits = 1 base; 3 bits = 2 bases
        if len(bit_rest) == 1:
            nucleotide_2 = "" # no nucleotide 2 to add
            homopolymer_check_bool = sequence[-3] == sequence[-2] == sequence[-1] 
        else: # rest = 3
            nucleotide_2 = bit_pair_to_dna[bit_rest[1:3]] # nucleotide_2 from a pair of bits
            homopolymer_check_bool = sequence[-3] == sequence[-2] == sequence[-1] or sequence[-2] == sequence[-1] == nucleotide_2 # true => careful for a potential homopolymer
        
        if homopolymer_check_bool:
            # need to break homopolymer with the nucleotide 1
            nucleotide_1 = bit_to_dna_balance_GC(bit_rest[0], sequence[-1])
        
        else:
            # adjust GC%
            check_window = sequence[-min(len(sequence), GC_window):] + nucleotide_2
            if check_window.count("A")+check_window.count("T") > check_window.count("G")+check_window.count("C"):
                nucleotide_1 = bit_to_dna_GC[bit_rest[0]]
            else:
                nucleotide_1 = bit_to_dna_AT[bit_rest[0]]
        
        sequence += nucleotide_1 + nucleotide_2
    
    # remove the banned words
    sequence_wo_banwords = remove_ban_words_baa_encoding(sequence)
    
    return sequence_wo_banwords


def dna_to_binary_baa(sequence: str) -> str:
    """
    convert back a sequence to binaries (opposite of binary_to_dna_baa)
    """
    
    binary_string = ""
    n_triplet, rest = divmod(len(sequence), 3)
    for i in range(0, n_triplet):
        triplet_nucleotide = sequence[i*3:(i+1)*3]
        bit_1 = dna_to_bit_ATGC[triplet_nucleotide[0]]
        bits_23 = dna_to_bit_pair[triplet_nucleotide[1]]
        bits_45 = dna_to_bit_pair[triplet_nucleotide[2]]

        binary_string += bit_1 + bits_23 + bits_45
    
    # handle rest < 3
    sequence_rest = sequence[n_triplet*3:n_triplet*3+rest]
    if len(sequence_rest) >= 1: # 1 base -> 1 bit
        binary_string += dna_to_bit_ATGC[sequence_rest[0]]
        
        if len(sequence_rest) == 2: # 2 bases -> 1 bit + 2 bits
            binary_string += dna_to_bit_pair[sequence_rest[1]]

    return binary_string


def remove_ban_words_baa_encoding(sequence: str, baa_method_offset=0) -> str:
    """
    remove banned words from a sequence encoded with the binary_conversion.binary_to_dna_baa() method
    the baa method ensure a GC% in [33;66] on a window of 3 bases, but we the constraint is on larger windows
    changing a base to avoid a banned word should not affect the whole sequence too much
    the changed base must be a beta base, and be A<=>G; T<=>C, so the decoding of the updated sequence will still return the same result
    
    if the original sequence has already been fragmented, the 3 bases windows of the baa method can be offset, baa_method_offset is used to correct this
    
    #TODO also remove inverse repeat regions of 10+ bases
    """
    
    # change a beta base but keep the binary meaning for the decoding
    dna_change_beta = {"A": "G", "T": "C", "G": "A", "C": "T"}

    # check for banned words
    indexes_ban_words = []
    for banned_word in sequence_control.get_forbidden_sequences():
        index = sequence.find(banned_word)
        while index != -1:
            indexes_ban_words.append([index, index+len(banned_word)])
            index = sequence.find(banned_word, index+1)
    
    if indexes_ban_words == []:
        return sequence
    
    # indexes of beta bases in the sequence (1st base every 3 base window)
    beta_indexes = [ baa_method_offset + 3*k for k in range(math.ceil(len(sequence)/3))]

    sequence_wo_bans = sequence

    for banned_words_indexes in indexes_ban_words:
        # indexes of beta bases that can be changed to remove a banned word
        potential_changes_index = [ k for k in beta_indexes if k >= banned_words_indexes[0] and k < banned_words_indexes[1] ]

        for change_index in potential_changes_index:
            sequence_wo_bans_list = list(sequence_wo_bans) # turn into list because python strings are immutable
            
            # change a beta_base without changing the meaning of encoded data
            sequence_wo_bans_list[change_index] = dna_change_beta[sequence_wo_bans[change_index]]
            
            index_before_ban_word = max(0, banned_words_indexes[0]-3) # test the changed part from 3 bases before to avoid creation of homopolymers
            index_after_ban_word = min(len(sequence)-1, banned_words_indexes[1]+3) # test to 3 bases after
            
            changed_sub_sequence = ''.join(sequence_wo_bans_list)[index_before_ban_word:index_after_ban_word]

            # test the sequence only for the banned words and homopolymers, ignore the GC%
            if sequence_control.sequence_check(changed_sub_sequence, window_size=60, min_GC=0, max_GC=100, verbose=False):   
                # keep the change that removes this ban word if it doesn't create homopolymers or other ban words            
                sequence_wo_bans = ''.join(sequence_wo_bans_list)
                #print("removed ban word", sequence[banned_words_indexes[0]:banned_words_indexes[1]],"->",changed_sub_sequence)
                break
            else:
                #print("failed to remove ban word", sequence[banned_words_indexes[0]:banned_words_indexes[1]],"-X>",changed_sub_sequence)
                #print("trying again...")
                pass
    
    # last check for very very odd cases (maybe overlapping ban words)
    for banned_word in sequence_control.get_forbidden_sequences():
        if banned_word in sequence_wo_bans:
            print("binary conversion: unable to remove forbidden sequences", sequence_wo_bans)
            exit(1)
    
    return sequence_wo_bans


def size_dna_from_bit_len_baa(bit_length):
    """
    assume binary to dna_baa conversion
    get the size the dna sequence will be from the given binary string length
    conversion rate is 1.66 bit/base
    """
    base_length = 3 * (bit_length//5) # 5 bits -> 3 bases
    rest = bit_length % 5
    if rest == 0:
        return base_length
    if rest == 1:
        return base_length + 1
    if rest == 3:
        return base_length + 2

    print("error size_of_dna_from_bit_len_baa : invalid size rest :",str(bit_length),"(rest of",str(rest)+")")
    exit(1)


def size_binary_from_dna_len_baa(dna_length):
    """
    assume dna_baa to binary conversion
    get the length the binary string will be from the given dna sequence
    conversion rate is 1.66 bit/base
    """
    base_length = 5 * (dna_length//3) # 3 bases -> 5 bits
    rest = dna_length % 3
    if rest == 0:
        return base_length
    if rest == 1:
        return base_length + 1
    if rest == 2:
        return base_length + 3
    

def test_conversion():
    
    for binary_size in range(100, 15000, 1):
        print("size",str(binary_size))
        random_binary = ""
        for i in range(binary_size):
             random_binary += random.Random().choice(['0', '1'])
        
        #random_binary = "00011110010"
        #seq_abaab = binary_to_dna_abaab(random_binary)
        seq_baa = binary_to_dna_baa(random_binary)
        if seq_baa is None:
            continue
        
        #decoded_abaab = dna_to_binary_abaab(seq_abaab)
        decoded_baa = dna_to_binary_baa(seq_baa)

        if size_binary_from_dna_len_baa(len(seq_baa)) != len(random_binary):
            print("dna",str(len(seq_baa)))
            exit(0)
        if size_dna_from_bit_len_baa(len(random_binary)) != len(seq_baa):
            print("bin",str(len(random_binary)))
            exit(0)

        #print("check abaab encoding", str(decoded_abaab == random_binary))
        #print("check baa encoding", str(decoded_baa == random_binary))
        """if not (decoded_baa == random_binary and sequence_control.sequence_check(seq_baa, min_GC=40, max_GC=60, verbose=True)):
            #print(random_binary)
            print(seq_baa)
            #print(decoded_baa)
            exit(1)"""
        
    #print("check abaab encoding")
    #sequence_control.sanity_check(seq_abaab)

    #print("check baa encoding")
    #sequence_control.sanity_check(seq_baa)
    
    
# =================== main ======================= #
if __name__ == '__main__':
    #doc_path = sys.argv[1]
    #binary_string = sys.argv[1]
    #seq = binary_to_dna_abaab(binary_string)
    #print(seq)
    #print(dna_to_binary_abaab(seq))
    test_conversion()

