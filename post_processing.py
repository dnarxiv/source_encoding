#!/usr/bin/python3

import argparse
import os
import subprocess

import pre_processing # also use the file_to_dna already imported here


"""
do the opposite of pre_processing.py
take dna sequences and convert them into files
"""


def convert_to_binary(input_dir_path, compressed_dir_path):
    """
    convert back the sequences into binary files
    """
    
    for filename in os.listdir(input_dir_path):
        file_path = os.path.join(input_dir_path, filename)
        result_file_path = os.path.join(compressed_dir_path, pre_processing.get_compressed_name(filename))
        
        # checking if it is a file
        if os.path.isfile(file_path):
            
            pre_processing.file_to_dna.decode_file(file_path, result_file_path) # TODO handle invalid checksum
        
        elif os.path.isdir(file_path):
            print("error post processing (convert_to_binary) : directory found in input_dir_path", filename)
            exit(0)
    

def uncompress_files(compressed_dir_path, uncompressed_dir_path):
    """
    unzip the files
    """
    for filename in os.listdir(compressed_dir_path):
        file_path = os.path.join(compressed_dir_path, filename)
        uncompressed_file_path = os.path.join(uncompressed_dir_path, pre_processing.get_uncompressed_name(filename))

        
        # checking if it is a file
        if os.path.isfile(file_path):
            pre_processing.unzip_file(file_path, uncompressed_file_path)
            
        elif os.path.isdir(file_path):
            print("error post processing (uncompress_files) : directory found in compressed_dir_path", filename)
            exit(0)
    
    
def replace_files(uncompressed_dir_path, original_files_dir_path):
    """
    recognize, extract and remove the headers
    replace the files in their original sub directories, group the split files together
    """
    
    split_files_dict = {} # dict to save the lines of subfiles that needs the lines of others related subfiles to write the complete file
    
    for filename in os.listdir(uncompressed_dir_path):
        file_path = os.path.join(uncompressed_dir_path, filename)
        result_file_path = os.path.join(original_files_dir_path, filename)
        
        # checking if it is a file
        if os.path.isfile(file_path):
            
            with open(file_path, "rb") as input_file:
                bytes_lines = input_file.readlines()
            
            # the decoded file can contain merged small files
            small_files = [] # list of lines associated to independent small files
            file_end_index = len(bytes_lines)
            
            for i, byte_line in enumerate(bytes_lines[::-1]): # loop over the lines ! from end to start ! 
                try:
                    if bytes("§*", 'utf-8') in byte_line: # characters that marks the beginning of a file header
                        #split_line = byte_line.split(bytes("§*", 'utf-8'))

                        file_start_index = len(bytes_lines)-i-1
                        small_file_bytes_lines = bytes_lines[file_start_index:file_end_index]
                        small_files.append(small_file_bytes_lines)
                        file_end_index = file_start_index+1 # move the index to the next line because the header can be after some previous file content one the same line
                except:
                    # decoding error for the line because the byte file is not a text file
                    pass
            
            for small_file_bytes_lines in small_files: # write each file

                # extract the abstract path from the file header line
                header_line = small_file_bytes_lines[0].split(bytes("§*", 'utf-8'))[1].decode().replace("\n", "")
                
                # if the header of the next file is at the end of the last line of this file, removes it
                small_file_bytes_lines[-1] = small_file_bytes_lines[-1].split(bytes("§*", 'utf-8'))[0]
                
                original_file_path = original_files_dir_path+"/"+header_line.split("__")[0] # extract the file abstracts path from the header
                original_directories, original_filename = os.path.split(os.path.abspath(original_file_path))
                
                try: # create the required directories of the path
                    os.makedirs(original_directories)
                except FileExistsError:
                    # directories already exists
                    pass
                
                if "__" in header_line: # means the file is a split file that needs to be merged with the corresponding other split files
                    # should be like "path/basename__1/3"
                    header_counter = header_line.split("__")[-1]
                    if "/" in header_counter: # if the '__' was in the basename of the file, it's not for the counter
                        subfile_number_str, subfile_total_str = header_counter.split("/") # get the number of splits and the split index for this subfile
                        subfile_number, subfile_total = int(subfile_number_str), int(subfile_total_str) # warning, subfile count starts from 1
                        
                        # get the list of subfiles lines for the same file, or init the list with size = number of subfiles
                        split_files_dict[original_file_path] = split_files_dict.get(original_file_path, ["" for i in range(subfile_total)])
                        
                        if split_files_dict[original_file_path][subfile_number-1] != "": # use subfile_number-1 because it starts from 1 and not 0
                            print("warning post processing (replace_files) : a subfile with the same number already exists",filename,original_file_path, subfile_number)
                        else:
                            # save the bytes of this subfile
                            split_files_dict[original_file_path][subfile_number-1] = b"".join(small_file_bytes_lines[1:])
                
                else:
                    # write the content without the header line in the file
                    with open(original_file_path, "wb") as output_file:
                        output_file.write(b"".join(small_file_bytes_lines[1:]))
    
        elif os.path.isdir(file_path):
            print("error post processing (replace_files) : directory found in uncompressed_dir_path", filename)
            exit(0)

    # merge the subfiles into complete files and write them
    for original_file_path, lines_list in split_files_dict.items():
        if "" in lines_list:
            print("warning post processing (replace_files) : incomplete subfile", original_file_path)
        else:
            with open(original_file_path, "wb") as output_file:
                output_file.write(b"".join(lines_list))

    
def post_processing(input_dir_path, output_dir_path):
    

    compressed_dir_path = output_dir_path + "/8_compressed"
    try:
        os.mkdir(compressed_dir_path)
    except OSError as error:
        print(error)
    
    convert_to_binary(input_dir_path, compressed_dir_path)
    
    uncompressed_dir_path = output_dir_path + "/9_uncompressed"
    try:
        os.mkdir(uncompressed_dir_path)
    except OSError as error:
        print(error)
        
    uncompress_files(compressed_dir_path, uncompressed_dir_path)
    
    original_files_dir_path = output_dir_path + "/10_original_directory"
    try:
        os.mkdir(original_files_dir_path)
    except OSError as error:
        print(error)
    
    replace_files(uncompressed_dir_path, original_files_dir_path)
    
    
# =================== main ======================= #
if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='process files of single dna sequence for dna reading to convert them into final files')
    parser.add_argument('-i', action='store', dest='input_dir_path', required=True,
                        help='path to input directory of fasta single sequences')
    parser.add_argument('-o', action='store', dest='output_dir_path', required=True,
                        help='path to the output files directory')


    # ---------- input list ---------------#
    arg = parser.parse_args()
    

    print("post-processing sequences...")
    
    post_processing(arg.input_dir_path, arg.output_dir_path)
        
    print("\tcompleted !")

