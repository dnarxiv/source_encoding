import sys
import math
import os
import inspect

currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.dirname(currentdir)+"/synthesis_modules")
import dna_file_reader as dfr

"""
methods to control if a sequence verify some dna constraints
"""


forbidden_sequences = ["GGTCTC", "GAGACC"] # constant, sequences that must be absent from any sequence, the reverse complement must also be in the list


def get_homopolymere_nbr(sequence: str, max_h: int) -> int:
    """
    count the number of homopolymeres larger than h_max in the sequence
    """
    h_nbr = 0 # number of homopolymere larger than h_max found
    row_size = 0 # size of the current row of consecutive nucleotides
    last_nucleotide = "" # previous nucleotide in the sequence
    for nucleotide in sequence:
        if nucleotide == last_nucleotide:
            row_size += 1
        else:
            if row_size > max_h:
                h_nbr += 1
            row_size = 1
        last_nucleotide = nucleotide
    if row_size > max_h:
        h_nbr += 1
        
    return h_nbr


def get_GC_percent(sequence: str, window_size: int) -> (float, float):
    """
    returns the minimum and maximum GC percentage for all the windows of the sequence
    """
    
    def check_GC_Window(window):
        A_content = window.count('A')
        C_content = window.count('C')
        T_content = window.count('T')
        G_content = window.count('G')
        
        GC_percent = round(100*(G_content + C_content) / (A_content + C_content + T_content + G_content), 1)
        return GC_percent
    
    if len(sequence) <= window_size:
        GC_percent = check_GC_Window(sequence)
        return GC_percent, GC_percent
        
    max_GC_percent = 0
    min_GC_percent = 100
    
    for index in range(len(sequence)-window_size):
        window = sequence[index:index+window_size]
        GC_percent = check_GC_Window(window)
        max_GC_percent = max(GC_percent, max_GC_percent)
        min_GC_percent = min(GC_percent, min_GC_percent)
        
    return min_GC_percent, max_GC_percent


def get_loop_nbr(sequence: str, loop_size: int, window_size: int) -> int:
    """
    count the number of potential loops (reverse complement of a sub sequence in a local window) in the sequence
    """
    loop_nbr = 0 # number of loop
    if len(sequence) < 2*loop_size: # sequence too short for any loop
        return 0
    
    for index in range(len(sequence)-loop_size):
        sub_sequence = sequence[index:index+loop_size]
        rev_compl_sub_sequence = dfr.reverse_complement(sub_sequence)
        if rev_compl_sub_sequence in sequence[index+loop_size:index+loop_size+window_size]:
            loop_nbr += 1

    return loop_nbr


def get_forbidden_sequences_nbr(sequence: str) -> bool:
    """
    check if some forbidden sequences are in the sequence
    """
    seq_nbr = 0
    for seq in forbidden_sequences:
        seq_nbr += sequence.count(seq)

    return seq_nbr
    

def sequence_check(sequence: str, window_size=60, h_size=3, min_GC=45, max_GC=55, verbose=False) -> bool:
    """
    test if a the conditions for a correct sequence are met, return True if all 4 constraints are valid
    """
    h_nbr = get_homopolymere_nbr(sequence, h_size)
    if verbose: print("number of homopolymere larger than",h_size,":",h_nbr)
    
    min_GC_percent, max_GC_percent = get_GC_percent(sequence, window_size)
    if verbose: print("GC percentage :",min_GC_percent,"% to",max_GC_percent,"%")
    
    loop_nbr = get_loop_nbr(sequence, 11, window_size)
    if verbose: print("number of potential loop :",loop_nbr)
    
    forbidden_sequences_nbr = get_forbidden_sequences_nbr(sequence)
    if verbose: print("number of forbidden_sequences :",forbidden_sequences_nbr)
    
    if h_nbr == 0 and min_GC_percent >= min_GC and max_GC_percent <= max_GC and loop_nbr == 0 and forbidden_sequences_nbr == 0:
        if verbose: print("sequence is correct")
        return True
    else:
        if verbose: print("sequence is not correct")
        return False
    
    
def get_forbidden_sequences() -> list:
    """
    just return the list of forbidden sequences
    """
    return forbidden_sequences
    

def count_repetitions(sequence: str, repetition_size: int = 8):
    
    repetition_dict = {}
    
    for k in range(10):
        for i in range(len(sequence)-repetition_size-k+1):
            seq_word = sequence[i:i+repetition_size+k]
            rc_seq_word = dfr.reverse_complement(sequence[i:i+repetition_size+k])
            
            repetition_dict[seq_word] = repetition_dict.get(seq_word, []) + [i]
            repetition_dict[rc_seq_word] = repetition_dict.get(rc_seq_word, []) + [i]
            
    copy_sequence = sequence
    
    repetition_count = 0
    for key, index_list in repetition_dict.items():
        if len(index_list) > 1:
            #print(key, str(index_list))
            repetition_count += 1
            
            for index in index_list:
                copy_sequence = copy_sequence[:index] + len(key)*"_" + copy_sequence[index+len(key):]
    

            
    #print(copy_sequence, str(len(copy_sequence)))
    print("repeted regions :",str(round(100*copy_sequence.count("_")/len(copy_sequence), 2)),"%")
    

def sanity_check(sequence: str):
    sequence_check(sequence, min_GC=40, max_GC=60, verbose=True)
    count_repetitions(sequence)
    

# =================== main ======================= #
if __name__ == '__main__':
     
    if len(sys.argv) != 2:
        print("usage : sequence_control.py sequence_path")
        sys.exit(1)

    # _, sequence = dfr.read_single_sequence_fasta(sys.argv[1])
    
    
    input_sequences = dfr.read_fasta(sys.argv[1])
    
    for fragment_name, fragment in input_sequences.items():
        print("control",fragment_name)
        #sequence_check(fragment, window_size=472, verbose=True)
        count_repetitions(fragment, 8)

